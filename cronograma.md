# Plan de Actividades


| Semana        | Actividad                               | Personas              |
| ----------- | ----------------------------------------- | --------------------- |
| 2/11        | [Reunión diseño experimentos](https://gitlab.com/pcremades/clorofila/issues/1)               | Carina, Leo, Federico           |
| 2/11        | [Convocatoria tesina](https://gitlab.com/pcremades/clorofila/raw/master/Documentos/Afiche_Tesina.pdf) | |
|             |              |        |
|             |                      |               |
|             |                      |               |
|             |                      |               |
|             |                      |               |
|             |                      |               |
