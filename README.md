# Clorofila

## Resumen
El nitrógeno (N) es el macronutriente que más limita el crecimiento de las plantas. Los rendimientos agrícolas dependen del estado nutricional 
de los cultivos y se encuentran directamente relacionados (dentro un rango) con el incremento de la fertilización nitrogenada. La utilización 
de fertilizantes nitrogenados es costosa, poco eficiente y puede traer aparejados problemas de contaminación ambiental asociados a su uso en exceso. 
En este sentido, es posible incrementar la eficiencia del uso del N (EUN) en los cultivos y producir alimentos bajo un marco de agricultura 
sustentable, realizando fertilizaciones nitrogenadas en las dosis necesarias y en los momentos fenológicos oportunos.
Una de las estrategias que se utilizan para incrementar la EUN es evaluar el estado nutricional de las plantas monitoreando la concentración 
de clorofilas en hojas, ya que esta se encuentra directamente relacionada con la concentración de N en estos tejidos. Sin embargo, los equipos 
portátiles actualmente disponibles para determinar clorofila en hojas son muy costosos (> USD 2000) y solo son accesibles para aquellas empresas 
de base agrícola que puedan afrontan el costo de inversión. Por lo tanto, el desarrollo de una metodología analítica y un equipo para la medición 
de clorofila que sea portátil y de bajo costo podría ser un aporte importante para el desarrollo agrícola sustentable de nuestra provincia y 
nuestro país. Este desarrollo se basará en la determinación de clorofilas por medio de un potenciostato y sensor electroquímico utilizando 
electrodos de grafito desechables modificados con nanomateriales. El desarrollo del equipo tendrá múltiples aplicaciones  en cada una de las 
instituciones participantes, intercambiando el tipo de electrodos. Todo el desarrollo será publicado bajo licencias de tecnologías libres.

### Cronograma de Actividades
[Cronograma](https://gitlab.com/pcremades/clorofila/blob/master/cronograma.md)

### Pruebas con potenciostato
![Link al WheeStat](https://gitlab.com/nanocastro/WheeStat6-Mza)